﻿/*
#* Intellic Integration LLC CONFIDENTIAL
#* __________________
#* 
#*  [2015] - [2020] Intellic Integration LLC 
#*  All Rights Reserved.
#* 
#* NOTICE:  All information contained herein is, and remains
#* the property of Intellic Integration LLC and its suppliers,
#* if any.  The intellectual and technical concepts contained
#* herein are proprietary to Intellic Integration LLC
#* and its suppliers and may be covered by U.S. and Foreign Patents,
#* patents in process, and are protected by trade secret or copyright law.
#* Dissemination of this information or reproduction of this material
#* is strictly forbidden unless prior written permission is obtained
#* from Intellic Integration LLC.
 */




using SecuGen.FDxSDKPro.Windows;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace FPScan
{
    public partial class Main : Form
    {

        private SGFPMDeviceName fpReaderDeviceName;
        private int fpReaderDeviceID;
        private int fpReaderError;
        private SGFingerPrintManager fpReaderManager;
        private SGFPMDeviceInfoParam fpReaderInfo = new SGFPMDeviceInfoParam();
        private string userName = string.Empty;
        private string clientName = Environment.MachineName.ToString();
        private int authReqId = -1;
        private string[] args = Environment.GetCommandLineArgs();
        private string authType;
        private bool matched = false;

        public Main()
        {
            InitializeComponent();

        }

        //Main_Load - this initializes the finger print scanner and searches for AuthReqid from ignition. 
        //done before screen loads
        private void Main_Load(object sender, EventArgs e)
        {
            if (args.Length > 1)
            {
                authType = args[1].ToString();
            }
            else
            {
                authType = "Logon";
            }

            //Get the AuthReqId for this workstation
            GetAuthReqId();

            //Initialize finger print scanner, exit application and surface error if not found
            FingerPrintInit();

        }

        private void Main_Shown(object sender, EventArgs e)
        {
            try
            {
                //check if we have an open auth request from ignition
                if (authReqId > 0)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (!matched)
                        {
                            //no match yet, try again
                            Console.WriteLine("Scanning for match");
                            FingerPrintScan();
                        }
                        else
                        {
                            //we have a match, get outta here!
                            break;

                        }
                    }
                }
                else
                {
                    //if we make it here we had no auth request!
                    MessageBox.Show("The system cannot find an authentication request number for machine: " + clientName.ToString() + " --:-- contact system administrator.");
                    Application.Exit();

                }


                if (!matched)
                {
                    SqlConnection connection = new SqlConnection(FPScan.Properties.Settings.Default.dbShopFoor.ToString());
                    using (connection)
                    {
                        connection.Open();
                        SqlCommand cmdUpdate = new SqlCommand(@"
                            update 
	                            AuthRequests
                            set 
	                            UserName = @UserName, 
	                            AuthConfirmed = @AuthConfirmed,
	                            EditedAt = @EditedDate,
	                            AuthType = @AuthType,
	                            EditedBy = @EditedBy, 
                                AuthRequestActive = @AuthRequestActive
                            where 
	                            AuthReqId = @AuthReqId", connection);
                        cmdUpdate.Parameters.AddWithValue("@UserName", DBNull.Value);
                        cmdUpdate.Parameters.AddWithValue("@AuthConfirmed", false);
                        cmdUpdate.Parameters.AddWithValue("@EditedDate", DateTime.Now.ToString());
                        cmdUpdate.Parameters.AddWithValue("@AuthType", authType);
                        cmdUpdate.Parameters.AddWithValue("@EditedBy", "Authenticator");
                        cmdUpdate.Parameters.AddWithValue("@AuthRequestActive", false);
                        cmdUpdate.Parameters.AddWithValue("@AuthReqId", authReqId);
                        cmdUpdate.ExecuteNonQuery();
                    }

                    //if we make it here we had an auth request, but no match!  Inform user and close
                    MessageBox.Show("The system cannot find a fingerprint match for the authentication request number " + authReqId.ToString() + " on machine: " + clientName.ToString() + "--:-- contact system administrator.");
                    Application.Exit();

                }
                else
                {
                    Application.Exit();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                Application.Exit();
            }

        }

        //FingerPrintInit - searches for and initialzes the finger print scanner properties. Exits application if nothing is found
        private void FingerPrintInit()
        {
            try
            {
                fpReaderDeviceName = SGFPMDeviceName.DEV_AUTO;
                fpReaderDeviceID = (int)(SGFPMPortAddr.USB_AUTO_DETECT);
                fpReaderManager = new SGFingerPrintManager();
                fpReaderError = fpReaderManager.Init(fpReaderDeviceName);
                fpReaderError = fpReaderManager.OpenDevice(fpReaderDeviceID);

                if (fpReaderError == (int)SGFPMError.ERROR_NONE)
                {
                    fpReaderError = fpReaderManager.GetDeviceInfo(fpReaderInfo);
                }
                else
                {
                    MessageBox.Show("Error - no fingerprint scanner found. (Is it unplugged?).  Press OK to continue.", "No Fingerprint Scanner Found");
                    Application.Exit();
                }

                if (fpReaderError != (int)SGFPMError.ERROR_NONE)
                {
                    MessageBox.Show(fpReaderError.ToString());
                    Application.Exit();
                }

                if (fpReaderError != (int)SGFPMError.ERROR_NONE)
                {
                    MessageBox.Show(fpReaderError.ToString());
                    Application.Exit();
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                Application.Exit();
            }

        }

        //FingerPrintScan - performs scan from fingerprint reader. reads scan data and compares to db records for match. 
        private void FingerPrintScan()
        {
            try
            {
                byte[] fpImage;
                byte[] imageTemplate;
                byte[] storedTemplate;
                fpImage = new byte[fpReaderInfo.ImageWidth * fpReaderInfo.ImageHeight];
                imageTemplate = new byte[400];
                storedTemplate = new byte[400];
                fpReaderError = fpReaderManager.GetImageEx(fpImage, 10000, pbFP.Handle.ToInt32(), 80);

                if (fpReaderError == 0)
                {
                    fpReaderError = fpReaderManager.CreateTemplate(null, fpImage, imageTemplate);
                    if (fpReaderError != (int)SGFPMError.ERROR_NONE)
                    {
                        MessageBox.Show(fpReaderError.ToString());
                        Application.Exit();
                    }
                    else
                    {
                        SqlConnection connection = new SqlConnection(FPScan.Properties.Settings.Default.dbShopFoor.ToString());
                        using (connection)
                        {
                            SqlCommand cmd = new SqlCommand(@"
                                SELECT 
                                    [User ID], 
                                    [Fingerprint Template] 
                                FROM 
                                    [Fingerprint Template Table] 
                                WHERE 
                                    [Active] = 1", connection);

                            connection.Open();
                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                if (rdr.HasRows)
                                {
                                    while (rdr.Read() && !matched)
                                    {
                                        storedTemplate = (byte[])rdr[1];
                                        fpReaderError = fpReaderManager.MatchTemplateEx(imageTemplate, SGFPMTemplateFormat.SG400, 0, storedTemplate, SGFPMTemplateFormat.SG400, 0, SGFPMSecurityLevel.NORMAL, ref matched);

                                        if (fpReaderError == (int)SGFPMError.ERROR_NONE)
                                        {
                                            if (matched)
                                            {

                                                //we have a match... get the username and update the database
                                                userName = rdr[0].ToString();
                                                SqlCommand cmdUpdate = new SqlCommand(@"
                                                update 
	                                                AuthRequests
                                                set 
	                                                UserName = @UserName, 
	                                                AuthConfirmed = @AuthConfirmed,
	                                                EditedAt = @EditedDate,
	                                                AuthType = @AuthType,
	                                                EditedBy = @EditedBy, 
                                                    AuthRequestActive = @AuthRequestActive
                                                where 
	                                                AuthReqId = @AuthReqId", connection);
                                                cmdUpdate.Parameters.AddWithValue("@UserName", userName);
                                                cmdUpdate.Parameters.AddWithValue("@AuthConfirmed", true);
                                                cmdUpdate.Parameters.AddWithValue("@EditedDate", DateTime.Now.ToString());
                                                cmdUpdate.Parameters.AddWithValue("@AuthType", authType);
                                                cmdUpdate.Parameters.AddWithValue("@EditedBy", "Authenticator");
                                                cmdUpdate.Parameters.AddWithValue("@AuthRequestActive", false);
                                                cmdUpdate.Parameters.AddWithValue("@AuthReqId", authReqId);
                                                cmdUpdate.ExecuteNonQuery();

                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show(fpReaderError.ToString());
                                            Application.Exit();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(fpReaderError.ToString());
                    Application.Exit();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                Application.Exit();
            }

        }

        //GetAuthReqID - queries the datbase for the most recent Authentication Request from this workstation
        private void GetAuthReqId()
        {
            try
            {
                //Query the database 
                SqlConnection connection = new SqlConnection(FPScan.Properties.Settings.Default.dbShopFoor.ToString());
                using (connection)
                {
                    SqlCommand cmd = new SqlCommand(@"
                    select top(1)
	                    AuthReqId
                    from 
	                    AuthRequests with (nolock)
                    where 
	                    ClientName = @ClientName
	                    and AuthConfirmed = 0
                        and AuthRequestActive = 1
                    order by 
	                    AuthReqId desc", connection);
                    cmd.Parameters.AddWithValue("@ClientName", clientName);
                    connection.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                authReqId = Convert.ToInt32(rdr["AuthReqId"]);
                            }
                        }
                        else
                        {
                            //if we make it here we had no auth request!
                            MessageBox.Show("The system cannot find an authentication request number for machine: " + clientName.ToString() + " --:-- contact system administrator.");
                            Application.Exit();

                        }
                    }


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                Application.Exit();
            }
        }

    }

}




