﻿namespace FPScan
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbFP = new System.Windows.Forms.PictureBox();
            this.lblClient = new System.Windows.Forms.Label();
            this.txtClientName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbFP)).BeginInit();
            this.SuspendLayout();
            // 
            // pbFP
            // 
            this.pbFP.BackColor = System.Drawing.SystemColors.Window;
            this.pbFP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbFP.Location = new System.Drawing.Point(12, 12);
            this.pbFP.Name = "pbFP";
            this.pbFP.Size = new System.Drawing.Size(166, 180);
            this.pbFP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFP.TabIndex = 0;
            this.pbFP.TabStop = false;
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Location = new System.Drawing.Point(13, 199);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(0, 13);
            this.lblClient.TabIndex = 1;
            // 
            // txtClientName
            // 
            this.txtClientName.AutoSize = true;
            this.txtClientName.Location = new System.Drawing.Point(56, 199);
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Size = new System.Drawing.Size(0, 13);
            this.txtClientName.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(191, 209);
            this.Controls.Add(this.txtClientName);
            this.Controls.Add(this.lblClient);
            this.Controls.Add(this.pbFP);
            this.Location = new System.Drawing.Point(1, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Authenticator";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Main_Load);
            this.Shown += new System.EventHandler(this.Main_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbFP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbFP;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label txtClientName;
    }
}

